// To create a class in JS, we use the class keyword and {}
// Naming convention for classes: Begins with Uppercase Characters.
/* 
    class Name {

    };
*/

class Dog {

    // We add a constructor method to a class to be able to initialize values upon instantiation of an object from a class.
    // "Instantiation" is the technical term when creating an object from a class
    // An object created from a class is called an instance.
    constructor(name,breed,age){

        this.name = name;
        this.breed = breed;
        this.age = age * 7;
    }
}

// instantiate a new object from a class
let dog1 = new Dog("Bantay","chihuahua",3);

console.log(dog1);

class Person {

    constructor(name,age,nationality,address){

        this.name = name;
        this.nationality = nationality;
        this.address = address;

        if(typeof age !== "number"){
            this.age = undefined;
        } else {
            this.age = age;
        }
    }

    greet(){
        console.log(`Hello! Good Morning!`);
        // this must be returned if the intended method can be chained
        return this;
    };
    introduce(){
        // We should be able to return this, so that when used in a chain, we can still have an access to the reference of this
        console.log(`Hi! My name is ${this.name}`)
        return this;
    };

    changeAddress(newAddress){
        // We can also update the field of our object by referring to the property via this keyword and reassigning a new value.
        this.address = newAddress;
        return this;
    };

}

let person1 = new Person("Clide",19,"British","503 Broadway London");
console.log(person1);

let person2 = new Person("Deuce",32,"American","4008 Ferry Street Huntsville Alabama");
console.log(person2);

// You can actually also chain methods from an instance
person1.greet().introduce()
person2.greet().introduce()


// Activity 1
class Student {
    
    constructor(name,email,grades){

        this.name = name;
        this.email = email;
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;

        grades.every(grade => {
            if(typeof grade !== "number"){
                return this.grades = undefined;
            } else{
                return this.grades = grades;
            }
        })
    };

    login(){
        console.log(`${this.email} has logged in`);
        return this;
    };

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    };

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    };

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => {
            return accumulator += num;
        })
        this.average = sum/4;
        return this;
    };

    willPass(){

        this.isPassed = this.average >= 85 ? true : false;
        return this;

    };

    willPassWithHonors(){

        this.isPassedWithHonors = this.willPass() && Math.round(this.average) >= 90 ? true : this.average >= 85 && this.average < 90 ? false : undefined;

        return this;
    };

}

let student1 = new Student("John","john@mail.com",[89,84,78,88]);
let student2 = new Student("Joe","joe@mail.com",[78,82,79,85]);
let student3 = new Student("Jane","jane@mail.com",[87,89,91,93]);
let student4 = new Student("Jessie","jessie@mail.com",[91,89,92,93]);
console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);

